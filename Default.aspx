﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label ID="Label1" runat="server" Text="Search in Twitter"></asp:Label>
            <br /> 
            <asp:TextBox ID="SearchBox" runat="server" ></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
            <br />
            <br />
            <asp:Repeater ID="RepeaterTweets" runat="server">
                <HeaderTemplate>
                    <table style="border-color: #006666; width: 100%; background-color: #f9f9f9;" cellspacing="1"
                        border="1">
                        <tr style="border-color: #006666; width: 100%; background-color: #bbbbbb;">
                            <th>ID
                            </th>
                            <th>Tweet
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#Eval("StatuseID") %>
                        </td>
                        <td>
                            <%#Eval("Tweet") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
