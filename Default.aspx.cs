﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void SearchButton_Click(object sender, EventArgs e) {
        List<TwitterStatuse> statuses = new List<TwitterStatuse>();

        string q = "";
        string result_type = "mixed";
        string count = "100";
        string lang = "en";

        q = SearchBox.Text;

        StatuseFactory factory = new StatuseFactory(q, "", "", "", result_type, count, lang);
        statuses = factory.Fact();

        RepeaterTweets.DataSource = statuses;
        RepeaterTweets.DataBind();
    }
}