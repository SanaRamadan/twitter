﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for TwitterConfiguration
/// </summary>
public class TwitterConfiguration
{
    private string consumerKey;
    private string consumerSecret;
    private string accessToken;
    private string accessTokenSecret;

    private string signatureMethod;
    private string version;
    private string nonce;
    private string timeStamp;


    public string ConsumerKey {
        get {
            consumerKey = "OgwGXXExgfRHpVNVycSvTdg52";
            return consumerKey;
        }
        set { consumerKey = value; }
    }

    public string ConsumerSecret {
        get {
            consumerSecret = "GVI29sd4A0v5LRlqBYImhxqB0JR1ww2chvOzt5OwaxyuEk0U25";
            return consumerSecret;
        }
        set { consumerSecret = value; }
    }

    public string AccessToken {
        get {
            accessToken = "3120687670-iMLZtyGu5N6AxKySQZZdde0Kck0PKwrLViW1UVW";
            return accessToken;
        }
        set { accessToken = value; }
    }

    public string AccessTokenSecret {
        get {
            accessTokenSecret = "CrH7g26Iw2X0elVGTv09uanFn6BhL9rZXR6OhMUXhf7aT";
            return accessTokenSecret;
        }
        set { accessTokenSecret = value; }
    }
    public string SignatureMethod {
        get {
            signatureMethod = "HMAC-SHA1";
            return signatureMethod;
        }
        set { signatureMethod = value; }
    }

    public string Version {
        get {
            version = "1.0";
            return version;
        }
        set { version = value; }
    }

    public string Nonce {
        get {
            nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            return nonce;
        }
        set { nonce = value; }
    }

    public string TimeStamp {
        get {
            TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            timeStamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
            return timeStamp;
        }
        set { timeStamp = value; }
    }

    public TwitterConfiguration() { }

    public TwitterConfiguration(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret,
     string signatureMethod, string version, string nonce, string timeStamp) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
        this.signatureMethod = signatureMethod;
        this.version = version;
        this.nonce = nonce;
        this.timeStamp = timeStamp;
    }
}