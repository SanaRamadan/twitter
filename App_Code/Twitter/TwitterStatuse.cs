﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TwitterStatuse
/// </summary>
public class TwitterStatuse
{
    private long statuseId;
    private string tweet;
    private DateTime createdDate;
    private float longitude;
    private float latitude;

    public long StatuseID { get { return statuseId; } set { statuseId = value; } }
    public string Tweet { get { return tweet; } set { tweet = value; } }
    public DateTime CreatedDate { get { return createdDate; } set { createdDate = value; } }
    public float Longitude { get { return longitude; } set { longitude = value; } }
    public float Latitude { get { return latitude; } set { latitude = value; } }
}